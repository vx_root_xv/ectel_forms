const formValues = (fields) => {
    let newObject = {}
    for (let [key, value] of Object.entries(fields)) {
        newObject[key] = value.value
    }
    return newObject
}

/* const $_hasValue = (b, prop) => {

   const item =  b.find(item => item.option_prop === prop)
    console.log(item)
    return item 
        ? item.option_value
        : '' 
} */

/* const propTest = (obj, prop) => {
  const testValue = obj[prop]
  console.log(testValue)
  return testValue !== undefined && !testValue && testValue
} */
export class User {
    constructor({
        first_name,
        last_name,
        company,
        company_type,
        website,
        business_number,
        sector,
        email,
        tel,
        address,
        city,
        country,
        /* business */
    }) {
        /*     this.sector = {
              'step': 1,
              'value': sector ? sector : '',
              'type': 'select',
              'label': 'Sector',
              'prop': 'sector',
              'layout': 'col s12 l12 m12',
              'v': false,
              'options': ['Accommodation', 'Transport', 'Transport Marine', 'Recreation and Entertainment', 'Exibitions MICE', 'Food and Beverage'],
            }, */

        this.personal_data = {
                'step': 1,
                'value': '',
                'type': 'label',
                'layout': 'col s12',
                'valid': false,
                'label': 'Representative Information'
            },
            this.first_name = {
                'step': 1,
                'value': first_name ? first_name : '',
                'type': 'text',
                'label': 'First Name',
                'prop': 'first_name',
                'layout': 'col s12 l6 m6',
                'valid': false,
                'v': true
            },
            this.last_name = {
                'step': 1,
                'value': last_name ? last_name : '',
                'type': 'text',
                'label': 'Last Name',
                'prop': 'last_name',
                'layout': 'col s12 l6 m6',
                'valid': false,
                'v': true
            },
            this.email = {
                'step': 1,
                'value': email ? email : '',
                'type': 'email',
                'label': 'Email',
                'prop': 'email',
                'layout': 'col s12',
                'valid': false,
                'v': true,
                'inDuplicate': true
            },
            this.tel = {
                'step': 1,
                'value': tel ? tel : '',
                'type': 'tel',
                'label': 'Contact',
                'prop': 'tel',
                'layout': 'col s12',
                'valid': false,
                'v': false
            },

            this.company_data = {
                'step': 2,
                'value': '',
                'type': 'label',
                'layout': 'col s12',
                'label': 'Company / Business information'
            },
            this.company = {
                'step': 2,
                'value': company ? company : '',
                'type': 'text',
                'label': 'Company / Business Name',
                'prop': 'company',
                'layout': 'col s12 ',
                'valid': false,
                'v': true
            },
            this.company_type = {
                'step': 2,
                'value': company_type ? company_type : '',
                'type': 'radio',
                'label': 'Company / Business Type',
                'prop': 'company_type',
                'layout': 'col s12 ',
                'valid': false,
                'v': false,
                'options': [
                    'Corporate', 'Unincorporated'
                ]
            },
            this.sector = {
                'step': 2,
                'value': sector ? sector : '',
                'type': 'select',
                'label': 'Sector',
                'prop': 'sector',
                'layout': 'col s12 l12 m12 admin-field',
                'v': false,
                'options': [
                    'Accommodation', 'Tours & Excursions ', 'Sites & Attractions', 'Car Rental', 'Tourism Taxi', 'Marine Tourism ', 'Destination Management Company', 'Other'
                ],
            },
            this.business_number = {
                'value': business_number,
                'step': 2,
                'type': 'text',
                'label': 'Company / Business Registration Number',
                'prop': 'business_number',
                'layout': 'col s12',
                'valid': false,
                'v': false
            },
            this.website = {
                'step': 2,
                'value': website ? website : '',
                'type': 'text',
                'label': 'Company Website',
                'prop': 'website',
                'layout': 'col s12',
                'valid': false,
                'v': false
            },
            this.address = {
                'step': 3,
                'value': address ? address : '',
                'type': 'textarea',
                'label': 'Address',
                'prop': 'address',
                'layout': 'col s12',
                'valid': false,
                'v': true
            },
            this.city = {
                'step': 3,
                'value': city ? city : '',
                'type': 'select',
                'label': 'City',
                'prop': 'city',
                'layout': 'col s12 l6 m6',
                'valid': false,
                'v': false,
                'options': ['Anse La Raye', 'Canaries', 'Castries', 'Chousiel', 'Dennery', 'Laborie', 'Gros Islet', 'Micoud', 'Vieux Fort', 'Rodney Bay', 'Soufriere'],
            },
            this.country = {
                'step': 3,
                'value': country ? country : 'Saint Lucia',
                'type': 'select',
                'label': 'Country',
                'prop': 'country',
                'layout': 'col s12 l6 m6',
                'valid': false,
                'options': ['Saint Lucia'],
                'v': false
            }
            /* this.business = {
              'step': 5,
              'prop': 'business',
              'label': 'Business',
              'conditional': this.sector,
              'type': 'group',
              'value':business ? business : [], //[{'option_key': '', option_prop': '','option_value': ''}],
              'options': [
                {
                  key: 'Accommodation',
                  fields: [
                    {
                      'value':business ? $_hasValue(business, 'business_Type') : '',// propTest(type, 'business_type') ? type.business_type : '',
                      'type': 'select',
                      'label': 'Business Type',
                      'prop': 'business_Type',
                      'layout': 'col s12',
                      'options': [ 'Hotel', 'Villa', 'Condo', 'Self-contained Apt', 'Cottage', 'Guest house', 'Inn'],
                      'v': false
                    },
                    {
                      'value': business ? $_hasValue(business, 'rooms') : '',
                      'type': 'number',
                      'label': 'Number of Rooms',
                      'prop': 'rooms',
                      'layout': 'col s12 l6 m6',
                      'v': true
                    },
                    {
                      'value': business ? $_hasValue(business, 'restaurants') : '',
                      'type': 'number',
                      'label': 'Number of Restaurants',
                      'prop': 'restaurants',
                      'layout': 'col s12 l6 m6',
                      'v': true
                    },
                    {
                      'value': business ? $_hasValue(business, 'bars') : '',
                      'type': 'number',
                      'label': 'Number of Bars',
                      'prop': 'bars',
                      'layout': 'col s12 l6 m6',
                      'v': true
                    },
                    {
                      'value': business ? $_hasValue(business, 'other') : '',
                      'type': 'checkbox',
                      'label': 'Other facilities',
                      'prop': 'other',
                      'layout': 'col s12 l12 m12',
                      'style' : 'single',
                      'v': false,
                      'options' : [ 'Spa', 'Conference', 'Pool', 'Fitness Center', 'Games Room', 'Salon']
                    }
                  ]
                }, 
                {
                  key: 'Transport',
                  fields: [
                    {
                      'value': '',
                      'type': 'number',
                      'label': 'Number of Vehicles in Fleet',
                      'prop': 'fleet',
                      'layout': 'col s12',
                      'v': true
                    },
                    {
                      'value': '',
                      'type': 'checkbox',
                      'label': 'Type of Operation',
                      'prop': 'type_of_operation',
                      'layout': 'col s12 l12 m12',
                      'style' : 'single',
                      'v': false,
                      'options' : ['Car rental', 'Taxi', 'Touring', 'Scooter/bikes/bicycles', 'Atv’s/Buggies/Go carts/ Golf carts']
                    }
                  ]
                },
                {
                  key: 'Transport Marine',
                  fields: [
                    {
                      'value': '',
                      'type': 'number',
                      'label': 'Number of boats in Fleet',
                      'prop': 'fleet',
                      'layout': 'col s12',
                      'v': true
                    },
                    {
                      'value': '',
                      'type': 'checkbox',
                      'label': 'Type of Operation',
                      'prop': 'type_of_operation',
                      'layout': 'col s12 l12 m12',
                      'style' : 'single',
                      'v': false,
                      'options' :['Jet skis', 'Snorkeling/Scuba diving', 'Sail boat', 'Deep sea/Sport fishing', 'Water Skiing', 'Kite surfing/Para sailing', 'Whale watching', 'Kayaking/ water cycles']
                    }
                  ]
                },
                {
                  key: 'Recreation and Entertainment', 
                  fields: [
                    {
                      'value': '',
                      'type': 'checkbox',
                      'label': 'Type of Operation',
                      'prop': 'type_of_operation',
                      'layout': 'col s12 l12 m12',
                      'style' : 'single',
                      'v': false,
                      'options' :['An amusement, theme recreational park or facility ', 'A cultural, historical site, an area of natural phenomenon or scenic beauty (e.g a botanical garden)', 'Hiking/riding trails', 'Agro-tourism initiatives', 'Botanical Garden', 'Casinos', 'Nightclubs', 'Golf /tennis facilities', 'Community -based Tourism initiatives ', 'An indoor/outdoor play or music show']
                    }
                  ]
                },
                {
                  key: 'Exibitions MICE',
                  fields: [
                    {
                      'value': '',
                      'type': 'checkbox',
                      'label': 'Type of Operation',
                      'prop': 'type_of_operation',
                      'layout': 'col s12 l12 m12',
                      'style' : 'single',
                      'v': false,
                      options: ['Conference facility', 'Events facility ', 'Tradeshows/Expositions/Familiarization tour', 'Cultural/Interpretation centers/Visitor booths', 'Promotional Materials', 'Hallmark Events ',]
                    }
                  ]      
                }
              ]
            } */
    }
    get _fullName() {
        return this.first_name.value + ' ' + this.last_name.value
    }
    set _fullName({
        first_name,
        last_name
    }) { // type Object
        this.first_name.value = first_name
        this.last_name.value = last_name
    }

    get _first_name() {
        return this.first_name.value
    }
    set _first_name(x) { // x type: String
        this.first_name.value = x
    }

    get _address() {
        return {
            'address': this.address.value,
            'city': this.city.value,
            'country': this.country.value
        }
    }
    set _address({
        address,
        city,
        country
    }) { //object
        this.address.value = address
        this.city.value = city
        this.country.value = country
    }

    get _validEmail() {
        return {
            'compare': this.email.value
        }
    }

    get _user() {
        return formValues(this)
    }
    set _user({
        first_name,
        last_name,
        address,
        city,
        email,
        sector,
        country,
        /* sector */
    }) { //object
        this.first_name.value = first_name
        this.last_name.value = last_name
        this.address.value = address
            // this.business.value = business
        this.sector.value = sector
        this.city.value = city
        this.country.value = country
        this.email.value = email
    }
}