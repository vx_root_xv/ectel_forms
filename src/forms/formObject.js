// QUICK QUIDE

// FORM OBJECT DEFAULTS
// {
//     name: `input_label_${this.index.toFixed(3)}`, <-- name of the field. Will contain the field value
//     label: "Label", <-- label for the field
//     placeholder: "", <-- placeholder for the field
//     type: "text", <-- type of field
//     isMulti: false, <-- multiple attribute for the field
//     isValid: true, <-- validity status of the field
//     showLabel: true, <-- show label or not
//     value: "", <-- default value for the field
//     validation: "required", <-- validation rules for the field || 'required|min:15|max:20|phone|email' <--- rules that are available as of now
//     wrapperStyle: {     <-- contains the class and style for the input wrapper div
//         class: "col s12",
//         style: {}
//     },
//     fieldStyle: { <-- contains the class and style for the field
//         class: "",
//         style: {}
//     },
//     validity: { <-- contains properties for the field error-success text
//         showSuccess: true <-- show success text
//         showError: true <--  show error text
//     },
//     helperText: "", <-- helper text message
//     icon: "", <-- icon for field
//     options: [ <--- select field options
//          {
//           text: ''  <-- option text
//           value: '' <-- option value
//          }
//      ] <--- select field options
// }




export const userForm = [{
        //Name
        name: "fullname",
        label: "Name",
        type: "text",
        isValid: false,
        showLabel: true,
        value: "",
        validation: "required|min:2|max:255",
        wrapperStyle: {
            class: "col s12 m6",
            style: {}
        },
        fieldStyle: {
            class: "indent"
        },
        validity: {
            showSuccess: true,
            showError: true,
        },
    },
    {
        name: "age",
        label: "Age",
        type: "number",
        isValid: false,
        showLabel: true,
        value: "",
        validation: "required",
        wrapperStyle: {
            class: "col s12 m2 offset-m3"
        },
        fieldStyle: {
            class: "indent",
        },
        validity: {
            showSuccess: true,
            showError: true,
        },

    },
    {

        name: "address",
        label: "Address",
        type: "text",
        isValid: false,
        showLabel: true,
        value: "",
        validation: "required|min:2|max:255",
        wrapperStyle: {
            class: "col s12 m6",
            style: {}
        },
        fieldStyle: {
            class: "indent"
        },
        validity: {
            showSuccess: true,
            showError: true,
        },
    },
    {
        name: "age",
        label: "Community",
        type: "text",
        isValid: false,
        showLabel: true,
        value: "",
        validation: "required",
        wrapperStyle: {
            class: "col s12 m6 "
        },
        fieldStyle: {
            class: "indent",
        },
        validity: {
            showSuccess: true,
            showError: true,
        },

    },
    {
        name: "cellphone",
        label: "Cell no.",
        placeholder: "",
        type: "number",
        showLabel: true,
        isValid: false,
        value: "",
        validation: "required|phone",
        wrapperStyle: {
            class: "col s12 m6"
        },
        validity: {
            showSuccess: true,
            showError: true,
        },
    },
    {
        name: "email",
        label: "Email address",
        placeholder: "",
        type: "email",
        isValid: true,
        showLabel: true,
        value: "",
        validation: "required|email",
        wrapperStyle: {
            class: "col s12 m6"
        },
        validity: {
            showSuccess: true,
            showError: true,
        },
    },
    {
        name: "loanNum",
        label: "Loan #",
        placeholder: "",
        type: "number",
        showLabel: true,
        isValid: false,
        value: "",
        validation: "required|numeric",
        wrapperStyle: {
            class: "col s12 m6"
        },
        validity: {
            showSuccess: true,
            showError: true,
        },
    },
    {
        name: "loanDate",
        label: "Date of Loan",
        type: "text",
        isValid: false,
        showLabel: true,
        value: "",
        validation: "required",
        wrapperStyle: {
            class: "col s12 m2 offset-m3"
        },
        fieldStyle: {
            class: "indent",
        },
        validity: {
            showSuccess: false,
            showError: true,
        },

    },
]
// export const testForm = [{
//         name: "first_name",
//         label: "First Name",
//         placeholder: "",
//         type: "text",
//         isValid: false,
//         showLabel: true,
//         value: "",
//         validation: "required|min:5|max:20",
//         wrapperStyle: {
//             class: "col s12 m6",
//             style: {}
//         },
//         fieldStyle: {
//             class: "indent"
//         },
//         validity: {
//             showSuccess: true,
//             showError: true,
//         },
//     },
//     {
//         name: "last_name",
//         label: "Last Name",
//         placeholder: "",
//         type: "text",
//         isValid: false,
//         showLabel: true,
//         value: "",
//         validation: "required|min:3|max:5",
//         validity: {
//             showSuccess: true,
//             showError: true,
//         },
//         wrapperStyle: {
//             class: "col s12 m6"
//         }
//     },
//     {
//         name: "email",
//         label: "Email",
//         placeholder: "",
//         type: "email",
//         isValid: false,
//         showLabel: true,
//         value: "",
//         validation: "required|email",
//         wrapperStyle: {
//             class: "col s12 m6"
//         },
//         validity: {
//             showSuccess: true,
//             showError: true,
//         },
//     },
//     {
//         name: "phonee",
//         label: "Phone #",
//         placeholder: "",
//         type: "number",
//         showLabel: true,
//         isValid: false,
//         value: "",
//         validation: "required|phone",
//         wrapperStyle: {
//             class: "col s12 m6"
//         },
//         validity: {
//             showSuccess: true,
//             showError: true,
//         },
//     },
//     {
//         name: "age",
//         label: "Age Range",
//         placeholder: "",
//         type: "select",
//         showLabel: true,
//         isValid: true,
//         value: "",
//         validation: "",
//         wrapperStyle: {
//             class: "col s12 m6"
//         },
//         options: [{
//                 text: "Choose Option",
//                 value: null
//             },
//             {
//                 text: "10-15",
//                 value: "a"
//             },
//             {
//                 text: "15-20",
//                 value: "b"
//             },
//             {
//                 text: "20-25",
//                 value: "c"
//             }
//         ]
//     },
//     {
//         name: "multi",
//         label: "Multiple",
//         placeholder: "",
//         type: "select",
//         showLabel: true,
//         isMulti: true,
//         isValid: true,
//         value: [],
//         validation: "required",
//         wrapperStyle: {
//             class: "col s12 m6"
//         },
//         options: [{
//                 text: "Choose Option",
//                 value: null
//             },
//             {
//                 text: "option 1",
//                 value: "a"
//             },
//             {
//                 text: "option 2",
//                 value: "b"
//             },
//             {
//                 text: "option 3",
//                 value: "c"
//             }
//         ]
//     }
// ]